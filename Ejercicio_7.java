﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A2_4;
import java.util.Scanner;
import java.lang.Math;

/**
 * 
 * COMENTARIO
 *
 * Programa que pide la combinación de acceso a una
 * caja fuerte. La combinación será un número de 4 cifras.
 * El programa pedirá la combinación y tenemos 4 intentos.
 * @author DAW118
 */
public class Ejercicio_7 {
    public static void main(String[] args) {
        // se inicia el teclado
        Scanner teclado = new Scanner(System.in);
        // se inicializan el número más grande y el más pequeño de 4 cifras
        int M = 9999;
        int N = 0;
        // se calcula un número aleatorio
        int x = (int) Math.floor(Math.random()*(M - N + 1)) + N;
        String m;
        if(x <= 9){
            m = "000" + x;
        } else if(x <= 99){
            m = "00" + x;
        } else if(x <= 999){
            m = "0" + x;
        } else {
            m = String.valueOf(x);
        }
        System.out.println("Número secreto: " + m);
        // te solicita un máximo de 4 veces un número de 4 cifras por teclado
        for(int i = 0; i < 4; i++){ 
            System.out.println("Introduzca la combinación para abrir la caja fuerte");
            // se lee el número
            int y = teclado.nextInt();
            String p = Integer.toString(y);
            if(y < 0){
                System.out.println("ERROR, INTRODUZCA UN NÚMERO POSITIVO"); 
            }
            if (p.equals(m)){ // si el número introducido es igual que el calculado aleatoriamente
                // se muestra por pantalla que se ha acertado
                System.out.println("La caja fuerte se ha abierto satisfactoriamente");
                i = 4; // se sale del programa
            } else { // si no se acierta
                // se muestra por pantalla los intentos restantes
                System.out.println("Lo siento, esa no es la combinación le quedan " + (4 - i -1) + " intentos");
            }
        }
    }
}
